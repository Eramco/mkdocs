![Logo](img/logo.jpg)
# Benvingut DAW

Desenvolupament d'aplicacions Web

## Asignatures
* [M01](m01.md) Sistemes informatics
* [M02](M02.md) Base de dades
* [M03](M03.md) Programació
* [M04](m04.md) Llenguatge Marques
* [M05](M05.md) Entorns de desenvolupament
* [M06](M06.md) Desenvolupament web en entorn Client
* [M07](m07.md) Desenvolupament web en entorn servidor
* [M08](m08.md) Desplegament d'aplicacions web
* [M09](m09.md) Disseny d'interfícies web
* [M10](m10.md) Projecte de desenvolupament d'aplicacions web

## Dades importants

    Direcció campus    # campus.iam.cat

[Bitbucket](https://bitbucket.org/Eramco/mkdocs)

[About](about.md)

