![Logo](img/logo.jpg)
#sistemas informáticos

*Professor: * **Ermengol**

*Hores setmanals: *4 hores setmanals, dimecres 11:30/13:30 i divendres 12:30/14:30**

**Lorem Ipsum** es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.

## Dades importants amb link inclós.

    Direcció campus    # campus.iam.cat
	About			   # [About](about.md)

[Link](http://campus.iam.cat/moodle/course/view.php?id=113)

